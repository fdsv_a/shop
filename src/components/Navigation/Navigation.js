import { NavLink } from "react-router-dom";
import styles from "./Navigation.module.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

const Navigation = () => {
  const cartCounter = useSelector((store) => store.counterReducer.cartCounter);
  const favoriteCounter = useSelector((store) => store.counterReducer.favoriteCounter);

  return (
    <div className={styles.header}>
      <NavLink to="/shop">
        <img src="./images/logo.png" alt="" />
      </NavLink>
      <div className={styles.container}>
        <NavLink to="/cart">
          <img src="./images/cart.svg" alt="" />
        </NavLink>
        <div>{cartCounter ? cartCounter : "0"}</div>
        <NavLink to="/favorites">
          <img src="./images/star.svg" alt="" />
        </NavLink>
        <div>
        {favoriteCounter ? favoriteCounter : "0"}
        </div>
      </div>
    </div>
  );
};

Navigation.propTypes = {
  cartCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  favoriteCounter: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
Navigation.defaultProps = {
  cartCounter: localStorage.getItem("cartCounter"),

  favoriteCounter: localStorage.getItem("favoriteCounter"),
};

export default Navigation;
