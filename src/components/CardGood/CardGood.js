import React from "react";
import styles from "./CardGood.module.scss";
import PropTypes from "prop-types";
import { ReactComponent as Favourite } from "./images/star.svg";
import { useState } from "react";
import { setModalApp } from "../../store/appReducer/actionCreators";
import { useDispatch } from "react-redux";

const CardGood = (props) => {
  const [favorite, setFavorite] = useState(false);
  const dispatch = useDispatch();

  const {
    title,
    price,
    image,
    addToFavorite,
    deleteFavorite,
    id,
    deleteCart,
    addCart,
  } = props;

  return (
    <div className={styles.card}>
      <span>
        <img src={image} alt="" />

        {deleteCart && (
          <div
            className={styles.closeBtn}
            onClick={() => {
              dispatch(setModalApp(id));
            }}
          >
            <span>&#10006;</span>
          </div>
        )}
      </span>
      <p className={styles.title}>{title}</p>
      <div className={styles.btnContainer}>
        <Favourite
          className={styles.favorite}
          onClick={() => {
            if (
              localStorage.getItem(id) &&
              localStorage.getItem(id).includes("favorite")
            ) {
              setFavorite(false);
              deleteFavorite(id);
            } else {
              setFavorite(true);
              addToFavorite(id);
            }
          }}
          fill={
            (localStorage.getItem(id) &&
              localStorage.getItem(id).includes("favorite")) ||
            favorite === true
              ? "yellow"
              : "none"
          }
        />
        <span>Add to favorites</span>
      </div>
      <div className={styles.priceContainer}>
        <p className={styles.price}>
          <nobr>{price} &#8372;</nobr>
        </p>
        {addCart && (
          <button
            type="button"
            onClick={() => {
              dispatch(setModalApp(id));
            }}
          >
            Add to cart
          </button>
        )}
      </div>
    </div>
  );
};

CardGood.propTypes = {
  title: PropTypes.string.isRequired,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  image: PropTypes.string,
  isFavorite: PropTypes.bool,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};
CardGood.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,
};

export default CardGood;
