import { useField } from "formik";
import styles from "./CustomInput.module.scss";

const CustomInput = (props) => {
  const [field, meta] = useField(props);
  const { type, placeholder } = props;
  return (
    <>
      <input {...field} type={type} placeholder={placeholder} />
      {meta.error && meta.touched && (
        <span className={styles.error}>{meta.error}</span>
      )}
    </>
  );
};

export default CustomInput;
