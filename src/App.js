import React, { useEffect } from "react";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import AppRoutes from "./AppRoutes";
import Preloader from "./components/Preloader/Preloader";
import { useDispatch, useSelector } from "react-redux";
import { getApp } from "./store/appReducer/actionCreators";
import {
  changeCartCounter,
  changeFavoriteCounter,
} from "./store/counterReducer/actionCreators";

const App = () => {
  const dispatch = useDispatch();
  const cartCounter = useSelector((store) => store.counterReducer.cartCounter);
  const favoriteCounter = useSelector(
    (store) => store.counterReducer.favoriteCounter
  );

  const addToCart = (id) => {
    let counter;
    cartCounter ? (counter = cartCounter) : (counter = 0);
    localStorage.getItem("cartCounter")
      ? (counter = parseInt(localStorage.getItem("cartCounter")) + 1)
      : (counter = cartCounter + 1);
    dispatch(changeCartCounter(counter));
    localStorage.setItem("cartCounter", JSON.stringify(counter));
    localStorage.getItem(id) === "favorite"
      ? (localStorage[id] = ["cart", "favorite"])
      : (localStorage[id] = "cart");
  };
  const deleteFromCart = (id) => {
    let counter;
    cartCounter ? (counter = cartCounter) : (counter = 0);
    localStorage.getItem("cartCounter")
      ? (counter = parseInt(localStorage.getItem("cartCounter")) - 1)
      : (counter = cartCounter - 1);
    dispatch(changeCartCounter(counter));
    localStorage.setItem("cartCounter", JSON.stringify(counter));
    localStorage.getItem(id).includes("cart") &&
    localStorage.getItem(id).includes("favorite")
      ? (localStorage[id] = "favorite")
      : localStorage.removeItem(id);
    localStorage.getItem("cartCounter") === '0' &&
      localStorage.removeItem("cartCounter");
  };

  const addToFavorite = (id) => {
    let counter;
    favoriteCounter ? (counter = favoriteCounter) : (counter = 0);
    localStorage.getItem("favoriteCounter")
      ? (counter = parseInt(localStorage.getItem("favoriteCounter")) + 1)
      : (counter = favoriteCounter + 1);
    dispatch(changeFavoriteCounter(counter));
    localStorage.setItem("favoriteCounter", JSON.stringify(counter));
    localStorage.getItem(id) === "cart"
      ? (localStorage[id] = ["cart", "favorite"])
      : (localStorage[id] = "favorite");
  };

  const deleteFavorite = (id) => {
    let counter;
    favoriteCounter ? (counter = favoriteCounter) : (counter = 0);
    localStorage.getItem("favoriteCounter")
      ? (counter = parseInt(localStorage.getItem("favoriteCounter")) - 1)
      : (counter = favoriteCounter - 1);
    dispatch(changeFavoriteCounter(counter));
    localStorage.setItem("favoriteCounter", JSON.stringify(counter));
    localStorage.getItem(id).includes(["cart", "favorite"])
      ? (localStorage[id] = "cart")
      : localStorage.removeItem(id);
    localStorage.getItem("favoriteCounter") === '0' &&
      localStorage.removeItem("favoriteCounter");
  };

  useEffect(() => {
    dispatch(getApp());
  }, [dispatch]);
  const goods = useSelector((store) => store.appReducer.data);
  const loading = useSelector((store) => store.appReducer.isLoading);
  return (
    <div className={styles.App}>
      <Header />
      {loading && <Preloader />}
      <div className={styles.container}>
        <AppRoutes
          goods={goods}
          addToFavorite={addToFavorite}
          deleteFavorite={deleteFavorite}
          addToCart={addToCart}
          deleteFromCart={deleteFromCart}
        />
      </div>
    </div>
  );
};

export default App;
