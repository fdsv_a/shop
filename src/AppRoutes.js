import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Favorites from "./pages/Favorites";

const AppRoutes = (props) => {
  const {
    goods,
    addToCart,
    deleteFromCart,
    addToFavorite,
    deleteFavorite,
    showModal,
    closeModal,
    modal,
  } = props;

  return (
    <Routes>
      <Route
        path="shop"
        element={
          <Home
            goods={goods}
            addToFavorite={addToFavorite}
            deleteFavorite={deleteFavorite}
            addToCart={addToCart}
            showModal={showModal}
            closeModal={closeModal}
            modal={modal}
          />
        }
      />
      <Route
        path="cart"
        element={
          <Cart
            goods={goods}
            addToFavorite={addToFavorite}
            deleteFavorite={deleteFavorite}
            addToCart={addToCart}
            deleteFromCart={deleteFromCart}
            showModal={showModal}
            closeModal={closeModal}
            modal={modal}
          />
        }
      />
      <Route
        path="favorites"
        element={
          <Favorites
            goods={goods}
            addToFavorite={addToFavorite}
            deleteFavorite={deleteFavorite}
            addToCart={addToCart}
            showModal={showModal}
            closeModal={closeModal}
            modal={modal}
          />
        }
      />
    </Routes>
  );
};
export default AppRoutes;
