import { GET_APP, SET_IS_LOADING_APP, SET_MODAL_APP, SET_CART_FORM_APP } from "./action";
import produce from "immer";

const initialState = {
  data: [],
  isLoading: false,
  modal: "",
  cartForm: false,
};
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_APP: {
      return produce(state, (draftState) => {
        draftState.data = action.payload;
      });

    }
    case SET_IS_LOADING_APP: {
      return produce(state, (draftState) => {
        draftState.isLoading = action.payload;
      });
    }
    case SET_MODAL_APP: {
      return produce(state, (draftState) => {
        draftState.modal = action.payload;
      });
    }
      case SET_CART_FORM_APP: {
        return produce(state, (draftState) => {
          draftState.cartForm = action.payload;
        });
      }
    default: {
      return state;
    }
  }
};

export default appReducer;
