import {
  GET_APP,
  SET_IS_LOADING_APP,
  SET_MODAL_APP,
  SET_CART_FORM_APP,
} from "./action";
import axios from "axios";

export const getApp = () => async (dispatch) => {
  dispatch({ type: SET_IS_LOADING_APP, payload: true });
  try {
    const { status, data } = await axios.get("./data.json");
    status === 200
      ? dispatch({ type: GET_APP, payload: data })
      : console.log("error");
  } catch (err) {
    console.log(err);
  }
  dispatch({ type: SET_IS_LOADING_APP, payload: false });
};

export const setModalApp = (payload) => (dispatch) => {
  dispatch({ type: SET_MODAL_APP, payload: payload });
};
export const setCartForm = (payload) => (dispatch) => {
  dispatch({ type: SET_CART_FORM_APP, payload: payload });
};
