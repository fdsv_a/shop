export const GET_APP = "GET_APP";
export const SET_IS_LOADING_APP = "SET_IS_LOADING_APP";
export const SET_MODAL_APP = "SET_MODAL_APP";
export const SET_CART_FORM_APP = "SET_CART_FORM_APP";
