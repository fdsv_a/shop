import React from "react";
import ListGoods from "../components/ListGoods/ListGoods";
import Modal from "../components/Modal/Modal";
import PropTypes from "prop-types";
import { setModalApp, setCartForm } from "../store/appReducer/actionCreators";
import { useSelector, useDispatch } from "react-redux";
import CartForm from "../components/CartForm/CartForm";
import styles from "./Cart.module.scss";

const Cart = (props) => {
  const { goods, addToFavorite, deleteFavorite, addToCart, deleteFromCart } =
    props;
  const modal = useSelector((store) => store.appReducer.modal);
  const cartForm = useSelector((store) => store.appReducer.cartForm);
  const cartCounter = useSelector((store) => store.counterReducer.cartCounter);

  const dispatch = useDispatch();

  return cartCounter ? (
    <>
      <ListGoods
        goods={goods.filter(
          (e) =>
            localStorage.getItem(e.id) &&
            localStorage.getItem(e.id).includes("cart")
        )}
        addToFavorite={addToFavorite}
        deleteFavorite={deleteFavorite}
        addToCart={addToCart}
        deleteFromCart={deleteFromCart}
        deleteCart={true}
        addCart={false}
      />
      <div className={styles.btnContainer}>
        <button
          onClick={(e) => {
            dispatch(setCartForm(true));
            setTimeout(() => {
              e.target.scrollIntoView({ block: "start", behavior: "smooth" });
            }, 100);
          }}
        >
          Order
        </button>
      </div>
      <div>
        {cartForm && (
          <CartForm
            goods={goods.filter(
              (e) =>
                localStorage.getItem(e.id) &&
                localStorage.getItem(e.id).includes("cart")
            )}
          />
        )}
      </div>
      {modal && (
        <Modal
          header="Do you want to delete this item from your shopping cart?"
          closeButton={true}
          text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, animi nisi dolorum illum culpa quae placeat rem numquam a consequatur reprehenderit facere voluptas nostrum corporis aut eaque dolor sunt aperiam?"
          actions={
            <>
              <button
                onClick={() => {
                  deleteFromCart(modal);
                  dispatch(setModalApp(""));
                }}
              >
                Ok
              </button>
              <button onClick={() => dispatch(setModalApp(""))}>Cancel</button>
            </>
          }
          closeModal={() => dispatch(setModalApp(""))}
        />
      )}
    </>
  ) : (
    <h1>Sorry, your cart is empty</h1>
  );
};

Cart.propTypes = {
  goods: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      image: PropTypes.string,
      isFavourite: PropTypes.bool,
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    })
  ),
  addToCart: PropTypes.func.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  addToFavorite: PropTypes.func.isRequired,
  deleteFavorite: PropTypes.func.isRequired,
};
Cart.defaultProps = {
  image: "./images/noimage.jpg",
  isFavorite: false,
};

export default Cart;
